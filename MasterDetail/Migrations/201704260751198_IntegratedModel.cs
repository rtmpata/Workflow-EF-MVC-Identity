namespace MasterDetail.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IntegratedModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkOrders", "CurrentWorkId", c => c.String(nullable: false, maxLength:128));
            AddColumn("dbo.WorkOrders", "CurrentWorker_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.WorkOrders", "CurrentWorker_Id");
            AddForeignKey("dbo.WorkOrders", "CurrentWorker_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkOrders", "CurrentWorker_Id", "dbo.AspNetUsers");
            DropIndex("dbo.WorkOrders", new[] { "CurrentWorker_Id" });
            DropColumn("dbo.WorkOrders", "CurrentWorker_Id");
            DropColumn("dbo.WorkOrders", "CurrentWorkId");
        }
    }
}
