// <auto-generated />
namespace MasterDetail.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CategoriesAdded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CategoriesAdded));
        
        string IMigrationMetadata.Id
        {
            get { return "201705170930168_CategoriesAdded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
