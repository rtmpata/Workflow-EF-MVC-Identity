﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MasterDetail.Models
{
    public class ServiceItem
    {
        public int ServiceItemId { get; set; }
        [Required(ErrorMessage = "You must enter an Item code.")]
        [StringLength(15, ErrorMessage = "The Item code must be 15 character or shorter.")]
        [Display(Name = "Item Code")]
        public string ServiceItemCode { get; set; }

        [Required(ErrorMessage = "You must enter a Name.")]
        [StringLength(80, ErrorMessage = "The name nust be 80 charater or shorter.")]
        [Display(Name = "Name")]
        public string ServiceItemName { get; set; }
        public decimal Rate { get; set; }
    }
}