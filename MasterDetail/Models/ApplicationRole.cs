﻿using MasterDetail.DataLayer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterDetail.Models
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole()
        {
           
        }
        public ApplicationRole(string name) : base(name)
        {

        }

       
    }
}