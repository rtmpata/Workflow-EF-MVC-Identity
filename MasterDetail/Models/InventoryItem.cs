﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MasterDetail.Models
{
    public class InventoryItem
    {
        public int InventoryItemId { get; set; }

        [Required(ErrorMessage = "You must enter an Item code.")]
        [StringLength(15, ErrorMessage ="The Item code must be 15 character or shorter.")]
        [Display(Name = "Item Code")]
        public string IventoryItemCode { get; set; }

        [Required(ErrorMessage ="You must enter a Name.")]
        [StringLength(80, ErrorMessage ="The name nust be 80 charater or shorter.")]
        [Display(Name = "Name")]
        public string IventoryItemName { get; set; }

        [Display(Name = "Unit Price")]
        public decimal UnitPrice { get; set; }
        public virtual Category Category { get; set; }

        [Display(Name = "Category")]
        public int CategoryId { get; set; }
    }
}