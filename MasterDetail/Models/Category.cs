﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TreeUtility;

namespace MasterDetail.Models
{
    public class Category : ITreeNode<Category>
    {
        public int Id { get; set; }

        private int? _parentCategoryId;
        public int? ParentCategoryId
        {
            get
            {
                return _parentCategoryId;
            }

            set
            {
                if (Id == value)
                    throw new InvalidOperationException("A category cannot have itself as its parent.");
                _parentCategoryId = value;
            }
        }


        [Required]
        [StringLength(20, ErrorMessage = "Category name can not be more than 20 character.")]
        [Display(Name ="Category")]
        public string CategoryName { get; set; }

        public IList<Category> Children { get; set; }

        //public virtual List<InventoryItem> InventoryItems { get; set; }

        public virtual Category Parent { get; set; }

       
    }
}