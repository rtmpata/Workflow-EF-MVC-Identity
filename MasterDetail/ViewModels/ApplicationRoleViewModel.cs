﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MasterDetail.ViewModels
{
    public class ApplicationRoleViewModel
    {
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage ="Your must enter a name for the Role.")]
        [StringLength(25, ErrorMessage ="Please enter a name with 25 character or less.")]
        [Display(Name = "Role Name")]
        public string Name { get; set; }
    }
}