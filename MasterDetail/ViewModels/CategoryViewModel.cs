﻿using MasterDetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MasterDetail.ViewModels
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public int? ParentCategoryId { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Category name can not be more than 20 character.")]
        [Display(Name = "Category")]
        public string CategoryName { get; set; }

        public virtual List<InventoryItem> InventoryItems { get; set; }
    }
}