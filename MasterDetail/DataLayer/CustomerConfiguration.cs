﻿using MasterDetail.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace MasterDetail.DataLayer
{
    public class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            Property(cc => cc.AccountNumber)
                .HasMaxLength(8)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("AK_Customer_AccountNumber") { IsUnique = true }));
            Property(cc => cc.CompanyName)
                .HasMaxLength(30)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("AK_Customer_CompanyName") { IsUnique = true }));
            Property(cc => cc.Address).HasMaxLength(30).IsRequired();
            Property(cc => cc.City).HasMaxLength(15).IsRequired();
            Property(cc => cc.State).HasMaxLength(2).IsRequired();
            Property(cc => cc.ZipCode).HasMaxLength(10).IsRequired();
            Property(cc => cc.Phone).HasMaxLength(15).IsOptional();
        }
    }
}