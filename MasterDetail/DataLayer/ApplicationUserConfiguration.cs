﻿using MasterDetail.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace MasterDetail.DataLayer
{
    public class ApplicationUserConfiguration : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserConfiguration()
        {
            Property(au => au.FirstName).HasMaxLength(25).IsOptional();
            Property(au => au.LastName).HasMaxLength(25).IsOptional();
            Property(au => au.Address).HasMaxLength(50).IsOptional();
            Property(au => au.City).HasMaxLength(25).IsOptional();
            Property(au => au.State).HasMaxLength(25).IsOptional();
            Property(au => au.LastName).HasMaxLength(10).IsOptional();
            Ignore(au => au.RoleList);
        }
    }
}